const express = require('express')
const {usuario} = require('./schemas/schema')
const cors = require('cors')
const app = express()
app.use(cors())
const port = 2000

app.get('/',function(req,res){
    res.send("Hola mundo hermoso :D")
})

app.get('/registrar',function(req,res){
    const user = new usuario({nombre: "Eduardo", edad:21, sexo: true})
    user.save()
    res.send("yei :D")
})

app.get('/mostrarUsuarios',async function(req,res){
    const usuarios = await usuario.find({}).exec()
    res.send(usuarios)
})

app.get('/eliminarUsuarios',async function(req,res){
    await usuario.find({}).remove().exec()
    const response =await usuario.find({}).exec()
    res.send(response)
})

app.get('/updateUsuarios',async function(req,res){
    await usuario.find({}).update({
        nombre:"don actualizadin"
    })
    const users = await usuario.find({}).exec()
    res.send(users)
})
app.listen(port,()=>console.log(`servidor corriendo en el puerto ${port}`))