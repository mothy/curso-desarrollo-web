const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/prueba1',{useNewUrlParser:true})

const usuarioSchema = mongoose.Schema({
    nombre: String,
    edad: Number,
    sexo: Boolean
})

const usuario = mongoose.model('usuario',usuarioSchema)

module.exports = {
    usuario
}